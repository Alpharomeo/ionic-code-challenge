# Angular Ionic - Form and listing

This code challenge is an example of a project that was completed in approximately three hours. Despite the limited timeframe, the project was built to the highest possible standard of quality and functionality. The challenge demonstrates the ability to work efficiently under pressure while still delivering a well-crafted solution.

## Steps to run the application

* Install the following prerequisites:
   * Node JS
   * Ionic
   * Angular
* Clone the repository to your local machine.
* Navigate to the project directory in your terminal/command prompt.
* Run the command 'npm install' to install dependencies.
* Run the command 'npx ionic serve' to launch the application.


# Screenshots:
![firstScreen](https://gitlab.com/Alpharomeo/ionic-code-challenge/-/raw/feat/initial/prints/screen-shot.png)

![secondScreen](https://gitlab.com/Alpharomeo/ionic-code-challenge/-/raw/feat/initial/prints/screen-shot2.png)

