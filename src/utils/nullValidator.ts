export default function findNullValues(json: any): string[] {
    const nullValues: string[] = [];
  
    Object.keys(json).forEach(key => {
      const value = json[key];
      if (value === null || value == "") {
        nullValues.push(key);
      } else if (typeof value === 'object') {
        const nestedNullValues = findNullValues(value);
        nullValues.push(...nestedNullValues.map(nestedKey => `${key}.${nestedKey}`));
      }
    });
  
    return nullValues;
  }