import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListProfilePage } from './list-profile.page';

const routes: Routes = [
  {
    path: '',
    component: ListProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListProfilePageRoutingModule {}
