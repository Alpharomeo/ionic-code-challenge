import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListProfilePageRoutingModule } from './list-profile-routing.module';

import { ListProfilePage } from './list-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListProfilePageRoutingModule
  ],
  declarations: [ListProfilePage]
})
export class ListProfilePageModule {}
