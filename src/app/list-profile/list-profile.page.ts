import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-profile',
  templateUrl: './list-profile.page.html',
  styleUrls: ['./list-profile.page.scss'],
})
export class ListProfilePage implements OnInit {
  public profiles!: any[]
  constructor() { }

  ngOnInit() {
    this.LoadProfiles()
  }
  async LoadProfiles(){
    const profiles = localStorage.getItem('profiles');
    if (profiles) {
      this.profiles = JSON.parse(profiles);
    }
  }
}
