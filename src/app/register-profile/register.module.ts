import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterPage } from './register.page';
import {IonicInputMaskModule} from "@thiagoprz/ionic-input-mask";
import { RegisterPageRouting } from './register-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterPageRouting,
    ReactiveFormsModule,
    IonicInputMaskModule
  ],
  declarations: [RegisterPage]
})
export class RegisterPageModule {}
