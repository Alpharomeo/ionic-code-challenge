import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { Camera, CameraResultType, CameraSource, Photo } from "@capacitor/camera";

import States from "../../utils/states"
import { Directory, Filesystem } from '@capacitor/filesystem';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { convertBlobToBase64 } from 'src/utils/blob';
import { LocalFile } from 'src/interface/LocalFile';
import findNullValues from 'src/utils/nullValidator';

const FILE_PATH = "temp"


@Component({
  selector: 'app-register',
  templateUrl: 'register.page.html',
  styleUrls: ['register.page.scss'],
})
export class RegisterPage implements OnInit {
  public firstName!: string | null;
  public lastName!: string | null;
  public birthdate!: Date | null;
  public localAddress!: string | null;
  public state!: string | null;
  public statesList!: string[];
  public jobTitle!: string | null;
  public image!: LocalFile | null;
  public formData!: FormGroup;
  public phone!: string | null;

  constructor(
    private route: ActivatedRoute,
    private platform: Platform,
    private loadingController: LoadingController,
    private toastController: ToastController) { }

  ngOnInit(): void {
    this.statesList = States.map(state => {
      return state.name
    })
    this.formData = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      birthdate: new FormControl(),
      localAddress: new FormControl(),
      state: new FormControl(),
      jobTitle: new FormControl(),
      phone: new FormControl()
    })


  }
  
  async onSubmit() {
    // TODO: We are going to store this information locally because I didn`t had enough time to make a API for it
    // But Ideally it should be saved in a API 
    const locallyStoredProfiles = localStorage.getItem('profiles');
    const payload = { ...this.formData.value, profilePic: this.image };
    const nullValues = findNullValues(payload);
    if (nullValues.length > 0 || !this.image) {
      const toast = await this.toastController.create({ message: "Please complete all fields before submitting.", position: "bottom", color: "danger", duration: 2000 })
      await toast.present();
      return;
    }
    const profiles = locallyStoredProfiles ? JSON.parse(locallyStoredProfiles) : [];
    profiles.push(payload);
    localStorage.setItem('profiles', JSON.stringify(profiles));
    const toast = await this.toastController.create({ message: "Your profile has been registered.    ", position: "bottom", color: "success", duration: 2000 })
    await toast.present().then(()=>{
      window.location.href = "/tabs/list-profile"
    });
  }

  async readFileData(resultImage: string) {
    const filePath = `${FILE_PATH}/${resultImage}`

    const readFile = await Filesystem.readFile({
      directory: Directory.Data,
      path: filePath
    })
    this.image = { data: `data:image/jpeg;base64, ${readFile.data}`, path: filePath, name: resultImage }
  }
  async loadFiles() {
    this.image = null
    const loading = await this.loadingController.create({ message: "Loading your image..." })
    await loading.present();

    Filesystem.readdir({
      directory: Directory.Data,
      path: `${FILE_PATH}`
    }).then(result => {
      const recentImage = result.files[result.files.length - 1]
      this.readFileData(recentImage.name)
    }, async err => {
      console.error("Error trying to get image:" + err)
      Filesystem.mkdir(
        {
          directory: Directory.Data,
          path: `${FILE_PATH}`
        }
      )
    }).then(_ => {
      loading.dismiss()

    })
  }

  async choosePictures() {
    const image = await Camera.getPhoto({
      quality: 1,
      resultType: CameraResultType.Uri,
      allowEditing: false,
      source: CameraSource.Photos
    });
    if (image) {
      this.saveImage(image)
    }
  }
  async readAsBase64(photo: Photo) {
    if (this.platform.is("hybrid")) {
      const file = await Filesystem.readFile({
        path: photo.path!
      })
      return file.data
    }
    else {
      const response = await fetch(photo.webPath!)
      const blob = await response.blob()

      return await convertBlobToBase64(blob) as string
    }
  }

  async saveImage(image: Photo) {
    const base64Image = await this.readAsBase64(image)
    const fileName = new Date().getTime() + ".jpeg";
    const savedFile = await Filesystem.writeFile({
      directory: Directory.Data,
      path: `${FILE_PATH}/${fileName}`,
      data: base64Image
    })
    try {
      this.loadFiles();

    }
    catch (err) {
      console.log(err)
     
    }
  }
}
