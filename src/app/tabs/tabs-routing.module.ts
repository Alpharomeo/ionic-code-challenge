import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
      {
        path: 'register',
        loadChildren: () => import('../register-profile/register.module').then( m => m.RegisterPageModule)
      },
      {
        path: 'list-profile',
        loadChildren: () => import('../list-profile/list-profile.module').then( m => m.ListProfilePageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
